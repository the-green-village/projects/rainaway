# Rainaway
This repository contains two python files: the rainaway_producer.py and the reference_producer.py file. They produce data for the rainaway setup and a reference setup, respectively. 

The data produced contains the temperature and the water content at 4 locations (2 at rainaway and 2 at reference locations) at 3 different depths per location: <br/>
Rainaway location A: -6 cm, -17 cm and -45 cm <br/>
Rainaway location B: -6 cm, -15 cm and -25 cm <br/>
Reference location A: -7 cm, -15 cm and -26 cm <br/>
Reference location B: -6 cm, -17 cm and -45 cm

# Installation
First, clone this project code from GitLab.
Secondly, create a virtual environment, and install the requirements:
```
python -m venv env
source env/bin/activate

pip install -r requirements.txt
```
Then, make the bash scripts executable
```
sudo chmod u+x rainaway_producer.sh reference_producer.sh
```

The Kafka producer runs as a cronjob on the Virtual Machine of The Green Village.

A message is uploaded (at a maximum frequency of) every hour from the sensors to Zentra Cloud. To allow for some slack, we make the script run at the 5th minute after every hour. To do this, edit crontab:

```
crontab -e
```

and add the following lines at the end of the file:
```
5 * * * * ~/producers/rainaway/code/rainaway_producer.sh > ~/log/rainaway_producer.txt 2>&1
5 * * * * ~/producers/rainaway/code/reference_producer.sh > ~/log/reference_producer.txt 2>&1
```

