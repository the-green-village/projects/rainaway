import requests
import pandas as pd

from datetime import timedelta
from tgvfunctions import tgvfunctions
from projectsecrets import RAINAWAY_DEVICE_SN, PROJECT_API_KEY, PROJECT_URL

token = "Token {TOKEN}".format(TOKEN=PROJECT_API_KEY)
headers = {'content-type': 'application/json', 'Authorization': token}
output_format = "df"

params = {'device_sn': RAINAWAY_DEVICE_SN, 'output_format': output_format}
response = requests.get(PROJECT_URL, params=params, headers=headers)
content = response.json()
df = pd.read_json(content['data'], convert_dates=False, orient='split')

# Make producer
producer = tgvfunctions.makeproducer('rainaway-producer')

# Create file with timestamp
begintime = tgvfunctions.timestamptofile('timestamp_rainaway', df["timestamp_utc"].iloc[0])

# Filter out most recent values
# Both the measurement and upload frequency are variable. 
# To make this script as robust as possible, read a saved file with the last 
# produced timestamp. Then it doesn't matter what the upload and measurement
# frequentcies are. It also doesn't matter how often this script is run. Every
# measurement is only produced once.
mask = (df["timestamp_utc"] > begintime) 
datapoints = df.loc[mask]

for index, row in datapoints.iterrows():
    if str(row['sensor_sn']) == 'T11-00014012':
        device_depth    = '. Depth: -6 cm.'
        device_location = ' at Rainaway location B'
    elif str(row['sensor_sn']) == 'T11-00015357':
        device_depth    = '. Depth: -15 cm.'
        device_location = ' at Rainaway location B'
    elif str(row['sensor_sn']) == 'T11-00015345':
        device_depth    = '. Depth: -25 cm.'
        device_location = ' at Rainaway location B'
    elif str(row['sensor_sn']) == 'T11-00015338':
        device_depth    = '. Depth: -6 cm.'
        device_location = ' at Rainaway location A'
    elif str(row['sensor_sn']) == 'T11-00015353':
        device_depth    = '. Depth: -17 cm.'
        device_location = ' at Rainaway location A'
    elif str(row['sensor_sn']) == 'T11-00015352':
        device_depth    = '. Depth: -45 cm.'
        device_location = ' at Rainaway location A'
    else:
        device_depth    = ''
        device_location = ''
    value = {
                "project_id": "soil_moisture",
                "application_id": RAINAWAY_DEVICE_SN,
                "device_id": str(row["sensor_sn"]),
                "device_type":  row["sensor_name"],
                "timestamp": row["timestamp_utc"]*1000,
                "measurements": [
                    {
                        "measurement_id": row["measurement"],
                        "measurement_description": row["measurement"] + device_location + device_depth,
                        "value": row["value"],
                        "unit": row["units"]
                    }
                ]
    }
    tgvfunctions.produce(producer, value)
producer.flush()
