#!/bin/bash
. ~/producers/.kafka_envs

cd ~/producers/rainaway/code
. ../env/bin/activate
python reference_producer.py
