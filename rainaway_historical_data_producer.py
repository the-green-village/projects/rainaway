import pandas as pd

import datetime as dt
from tgvfunctions import tgvfunctions

#df = pd.read_csv ('historical_data.csv', parse_dates=['time'], date_parser=dateparse)
datapoints = pd.read_csv ('historical_data_2.csv')

# Make producer
producer = tgvfunctions.makeproducer('rainaway-producer')

for index, row in datapoints.iterrows():
    timestamp = dt.datetime.strptime( row["time"], '%m/%d/%Y %H:%M:%S').timestamp()*1000
    # Port1
    sensor_sn       = 'T11-00015352'
    sensor_name     = 'TEROS 11'
    device_depth    = '. Depth: -45 cm.'
    device_location = ' at Rainaway location A'
    values = {
                "project_id": "soil_moisture",
                "application_id": "z6-14214",
                "device_id": sensor_sn,
                "device_type": sensor_name,
                "timestamp": timestamp,
                "measurements": [
                    {
                        "measurement_id": 'Soil Temperature',
                        "measurement_description": 'Soil Temperature' + device_location + device_depth,
                        "value": row["Port1.1"],
                        "unit": ' °C'
                    },
                    {
                        "measurement_id": 'Water Content',
                        "measurement_description": 'Water Content' + device_location + device_depth,
                        "value": row["Port1"],
                        "unit": ' m³/m³'
                    }
                ]
    }
    tgvfunctions.produce(producer, values)

    # Port2
    sensor_sn       = 'T11-00015353'
    sensor_name     = 'TEROS 11'
    device_depth    = '. Depth: -17 cm.'
    device_location = ' at Rainaway location A'
    values = {
                "project_id": "soil_moisture",
                "application_id": "z6-14214",
                "device_id": sensor_sn,
                "device_type": sensor_name,
                "timestamp": timestamp,
                "measurements": [
                    {
                        "measurement_id": 'Soil Temperature',
                        "measurement_description": 'Soil Temperature' + device_location + device_depth,
                        "value": row["Port2.1"],
                        "unit": ' °C'
                    },
                    {
                        "measurement_id": 'Water Content',
                        "measurement_description": 'Water Content' + device_location + device_depth,
                        "value": row["Port2"],
                        "unit": ' m³/m³'
                    }
                ]
    }
    tgvfunctions.produce(producer, values)

    # Port3
    sensor_sn       = 'T11-00015338'
    sensor_name     = 'TEROS 11'
    device_depth    = '. Depth: -6 cm.'
    device_location = ' at Rainaway location A'
    values = {
                "project_id": "soil_moisture",
                "application_id": "z6-14214",
                "device_id": sensor_sn,
                "device_type": sensor_name,
                "timestamp": timestamp,
                "measurements": [
                    {
                        "measurement_id": 'Soil Temperature',
                        "measurement_description": 'Soil Temperature' + device_location + device_depth,
                        "value": row["Port3.1"],
                        "unit": ' °C'
                    },
                    {
                        "measurement_id": 'Water Content',
                        "measurement_description": 'Water Content' + device_location + device_depth,
                        "value": row["Port3"],
                        "unit": ' m³/m³'
                    }
                ]
    }
    tgvfunctions.produce(producer, values)

    # Port4
    sensor_sn       = 'T11-00015345'
    sensor_name     = 'TEROS 11'
    device_depth    = '. Depth: -25 cm.'
    device_location = ' at Rainaway location B'
    values = {
                "project_id": "soil_moisture",
                "application_id": "z6-14214",
                "device_id": sensor_sn,
                "device_type": sensor_name,
                "timestamp": timestamp,
                "measurements": [
                    {
                        "measurement_id": 'Soil Temperature',
                        "measurement_description": 'Soil Temperature' + device_location + device_depth,
                        "value": row["Port4.1"],
                        "unit": ' °C'
                    },
                    {
                        "measurement_id": 'Water Content',
                        "measurement_description": 'Water Content' + device_location + device_depth,
                        "value": row["Port4"],
                        "unit": ' m³/m³'
                    }
                ]
    }
    tgvfunctions.produce(producer, values)

    # Port5
    sensor_sn       = 'T11-00015357'
    sensor_name     = 'TEROS 11'
    device_depth    = '. Depth: -15 cm.'
    device_location = ' at Rainaway location B'
    values = {
                "project_id": "soil_moisture",
                "application_id": "z6-14214",
                "device_id": sensor_sn,
                "device_type": sensor_name,
                "timestamp": timestamp,
                "measurements": [
                    {
                        "measurement_id": 'Soil Temperature',
                        "measurement_description": 'Soil Temperature' + device_location + device_depth,
                        "value": row["Port5.1"],
                        "unit": ' °C'
                    },
                    {
                        "measurement_id": 'Water Content',
                        "measurement_description": 'Water Content' + device_location + device_depth,
                        "value": row["Port5"],
                        "unit": ' m³/m³'
                    }
                ]
    }
    tgvfunctions.produce(producer, values)

    # Port6
    sensor_sn       = 'T11-00014012'
    sensor_name     = 'TEROS 11'
    device_depth    = '. Depth: -6 cm.'
    device_location = ' at Rainaway location B'
    values = {
                "project_id": "soil_moisture",
                "application_id": "z6-14214",
                "device_id": sensor_sn,
                "device_type": sensor_name,
                "timestamp": timestamp,
                "measurements": [
                    {
                        "measurement_id": 'Soil Temperature',
                        "measurement_description": 'Soil Temperature' + device_location + device_depth,
                        "value": row["Port6.1"],
                        "unit": ' °C'
                    },
                    {
                        "measurement_id": 'Water Content',
                        "measurement_description": 'Water Content' + device_location + device_depth,
                        "value": row["Port6"],
                        "unit": ' m³/m³'
                    }
                ]
    }
    tgvfunctions.produce(producer, values)

    # Port7
    sensor_sn       = 'None'
    sensor_name     = 'Battery'
    device_depth    = ''
    device_location = ''
    values = {
                "project_id": "soil_moisture",
                "application_id": "z6-14214",
                "device_id": sensor_sn,
                "device_type": sensor_name,
                "timestamp": timestamp,
                "measurements": [
                    {
                        "measurement_id": 'Battery Percent',
                        "measurement_description": 'Battery Percent' + device_location + device_depth,
                        "value": float(row["Port7"]),
                        "unit": '%'
                    },
                    {
                        "measurement_id": 'Battery Voltage',
                        "measurement_description": 'Battery Voltage' + device_location + device_depth,
                        "value": float(row["Port7.1"]),
                        "unit": ' mV'
                    }
                ]
    }
    tgvfunctions.produce(producer, values)

    # Port8
    sensor_sn       = '1628793681'
    sensor_name     = 'Battery'
    device_depth    = ''
    device_location = ''
    values = {
                "project_id": "soil_moisture",
                "application_id": "z6-14214",
                "device_id": sensor_sn,
                "device_type": sensor_name,
                "timestamp": timestamp,
                "measurements": [
                    {
                        "measurement_id": 'Reference Pressure',
                        "measurement_description": 'Reference Pressure' + device_location + device_depth,
                        "value": row["Port8"],
                        "unit": ' kPa'
                    },
                    {
                        "measurement_id": 'Logger Temperature',
                        "measurement_description": 'Logger Temperature' + device_location + device_depth,
                        "value": row["Port8.1"],
                        "unit": ' °C'
                    }
                ]
    }
    tgvfunctions.produce(producer, values)


producer.flush()
